# Saps makefile
#       __ 
#   |_|(_   
#   | |__)
#	Author: Håvard Syslak 
#   Date: 31.01.2021   

TARGET := bin/saps

ifeq (run,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

$(TARGET): $(shell find . -name "*.go")
	@mkdir -p bin
	go build -o $(TARGET) *.go

build:
	@mkdir -p bin
	go build -o $(TARGET) *.go

get: 
	@echo "Installing dependencies"
	go get

run: build
	./bin/saps $(RUN_ARGS)

all: get build	

install:
	@echo "installing saps"
	$(shell mkdir -p $$XDG_CONFIG_HOME/saps)
# $(shell if ! grep -q 'var usr_config_home' config.go; then echo $$XDG_CONFIG_HOME | awk '{ print "var usr_config_home = \""$$1"\""}' >> config.go; fi)
# $(shell if ! grep -q 'var templatePath' config.go; then echo $$XDG_CONFIG_HOME/saps | awk '{ print "var templatePath = \""$$1"\""}' >> config.go; fi)
	$(shell cp -r -i .config/saps $$XDG_CONFIG_HOME/)
	$(shell if [ ! -f $$XDG_CONFIG_HOME/saps/sapsrc ]; then echo $$USER | awk '{print "# Saps config file\nset AUTHOR "$$1""}' >> $$XDG_CONFIG_HOME/saps/sapsrc; fi )
	@make build
	@sudo ln -s -f $(shell pwd)/bin/saps /usr/bin/saps
	@echo Done!

uninstall:
	@echo "Uninstalling saps"
	@sudo rm -rf /usr/bin/saps
	@echo "Done!"

