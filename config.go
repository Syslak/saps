package main

/*	Project: saps
	     __
	 |_|(_
	 | |__)
	Author: Håvard Syslak
	Date: 31.01.2021
*/

// Global vars used as defaults.
var promptNewProject = true // Prompts for new project if .saps folder found in parent dir

// Golang specific
var goGit = true
var goMake = true

//Latex specific
var latexGit = false
var latexMake = false

//C specific
var cGit = true
var cMake = true

var usr_config_home string
var templatePath = ""
