package main

/*	Project: saps
		 __
	 |_|(_
	 | |__)
	Author: Håvard Syslak
	Date: 19.02.2021
*/
var version string = "saps 0.0.1"

var usage string = `usage: 	saps <folder> <language> [-t, --template template] [--git, -g bool] [--make, -m bool]
				 [-p, --package package] [-f, --filename filename]

	saps <filename> [-t, --template template] [--git, -g bool] [--make, -m bool] 
		        [-p, --package package] [-f, --filename filename]`

var set = make(map[string]string) // Map for the vars to "replace" in template eg. $AUTHOR$
var newProject bool = false
var home string

type arguments struct {
	isFolder             bool
	fileType             string
	specifiedFileName    string
	projectName          string
	specifiedTemplate    string
	gitignore            bool
	gitFlag              bool
	overrideGitflag      bool
	makefile             bool
	makeFileFlag         bool
	overrideMakefileflag bool
	packageFlag          bool
	srcFolder            bool
	gitDefault           bool
	makeDefault          bool
}
