package main

/*	Project: saps
	     __
	 |_|(_
	 | |__)
	Author: Håvard Syslak
	Date: 31.01.2021
*/
import (
	"os"
	"fmt"
)

func main() {

	var err error
	usr_config_home, err = os.UserConfigDir()
	if err != nil {
		fmt.Println(err)
		return
	}

	if templatePath == "" {
		templatePath = usr_config_home + "/saps"
	}
	args := os.Args[1:]
	home, err = os.UserHomeDir()
	if err != nil {
		fmt.Println(err)
		return

	}

	// parseArgs()
	var parsedArgs arguments
	parsedArgs = parseArgs(args)

	makeProject(&parsedArgs)
}
