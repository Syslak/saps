package main

/*	Project: saps
		__
	|_|(_
	| |__)
	Author: Håvard Syslak
	Date: 31.01.2021
*/

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

// TODO make language specific gitignore files
func parseArgs(args []string) arguments {

	// Defaults
	parsedArgs := arguments{
		projectName:          "",
		specifiedFileName:    "",
		specifiedTemplate:    "default",
		overrideGitflag:      false,
		makeFileFlag:         false,
		overrideMakefileflag: false,
		packageFlag:          false,
	}
	var split []string
	var flags []string

	// fmt.Println(reflect.TypeOf(args))
	//fmt.Println(args)

	if len(args) > 0 {
		split = strings.Split(args[0], ".")

	} else {
		fmt.Println(usage)
		os.Exit(0)
	}
	for _, erg := range split {
		switch erg {
		case "-v", "--version":
			fmt.Println(version)
			os.Exit(0)
		case "-h", "--help":
			fmt.Println(usage)
			os.Exit(0)
		}
	}
	if len(split) == 1 {
		parsedArgs.isFolder = true
		parsedArgs.projectName = args[0]
		parsedArgs.fileType = args[1]
		flags = args[2:]

	} else {
		//fmt.Println("Split:", split[0])
		parsedArgs.isFolder = false
		parsedArgs.fileType = split[1]
		parsedArgs.projectName = split[0]
		flags = args[1:]
	}

	for i, flag := range flags {

		if i >= 1 {
			if flag[0] != '-' && flags[i-1][0] == '-' {
				continue
			}
		}

		switch flag {
		case "-t", "--template":
			parsedArgs.specifiedTemplate = flags[i+1]
			fmt.Println("template: ", parsedArgs.specifiedTemplate)

		case "-g", "--git":
			parsedArgs.gitFlag = true

			if len(flags) >= i+2 {

				if flags[i+1][0] != '-' {

					if flags[i+1] == "false" {
						parsedArgs.gitFlag = false
						parsedArgs.overrideGitflag = true

					} else if flags[i+1] == "true" {
						parsedArgs.gitFlag = true
						parsedArgs.overrideGitflag = true
					} else {
						fmt.Println("Unknown arg for --git.", "Usage:\n--git true/flase\n-g true/false\n-g\n --git")
						os.Exit(0)
					}
				}

			}

		case "-f", "--filename", "--file":
			parsedArgs.specifiedFileName = flags[i+1]
			fmt.Println("filenamn: ", parsedArgs.specifiedFileName)

		case "-m", "--make", "--makefile":
			parsedArgs.makeFileFlag = true

			if len(flags) >= i+2 {

				if flags[i+1][0] != '-' {

					if flags[i+1] == "false" {
						parsedArgs.makeFileFlag = false
						parsedArgs.overrideMakefileflag = true

					} else if flags[i+1] == "true" {
						parsedArgs.makeFileFlag = true
						parsedArgs.overrideMakefileflag = true
					} else {
						fmt.Println("Unknown arg for --make.", "Usage:\n--git true/flase\n-g true/false\n-g\n --git")
						os.Exit(0)
					}
				}

			}

		case "-p", "--package":
			parsedArgs.packageFlag = true
			set["PACKAGE"] = flags[i+1]

		default:
			fmt.Println("Unknown flag", flag)
			os.Exit(1)
		}

	}

	switch parsedArgs.fileType {
	case "tex", "latex":
		parsedArgs.fileType = "tex"
		parsedArgs.srcFolder = false
		parsedArgs.gitDefault = latexGit
		parsedArgs.makeDefault = latexMake

		// gitignore? default for latex is false
		// parsedArgs.gitignore = gitOrMake(parsedArgs.gitFlag, parsedArgs.overrideGitflag, parsedArgs.gitDefault, parsedArgs.isFolder)

		// Makefile? TODO: make Makefile for latex
		// parsedArgs.makefile = gitOrMake(parsedArgs.makeFileFlag, parsedArgs.overrideMakefileflag, parsedArgs.makeDefault, parsedArgs.isFolder)

	case "go", "golang":
		parsedArgs.fileType = "go"
		parsedArgs.srcFolder = false
		parsedArgs.gitDefault = goGit
		parsedArgs.makeDefault = goMake

		// gitignore? default for go is true
		// parsedArgs.gitignore = gitOrMake(parsedArgs.gitFlag, parsedArgs.overrideGitflag, parsedArgs.gitDefault, parsedArgs.isFolder)

		// Makefile? default for go projects is true
		// parsedArgs.makefile = gitOrMake(parsedArgs.makeFileFlag, parsedArgs.overrideMakefileflag, parsedArgs.makeDefault, parsedArgs.isFolder)

		if parsedArgs.packageFlag == false {
			fmt.Print("package main? [Y/n] ")
			var specifiedPackage string
			fmt.Scanln(&specifiedPackage)
			set["PACKAGE"] = "main"

			if len(specifiedPackage) > 0 {
				if strings.ToLower(specifiedPackage)[0] == 'n' {
					set["PACKAGE"] = parsedArgs.projectName
				}
			}
		}

	case "c", "C":
		parsedArgs.srcFolder = false
		parsedArgs.gitDefault = cGit
		parsedArgs.makeDefault = cMake

		// gitignore? default for cprojects is true
		// parsedArgs.gitignore = gitOrMake(parsedArgs.gitFlag, parsedArgs.overrideGitflag, parsedArgs.gitDefault, parsedArgs.isFolder)
		// Makefile? default for c projects is true
		// parsedArgs.makefile = gitOrMake(parsedArgs.makeFileFlag, parsedArgs.overrideMakefileflag, parsedArgs.makeDefault, parsedArgs.isFolder)

		parsedArgs.fileType = "c"

	default:
		fmt.Println("Unknown filetype: ", parsedArgs.fileType)
		os.Exit(0)

	}
	return parsedArgs
}

func readConfig(localConfigPath []string, projectName string, hasLocalConfig bool, folder bool) map[string]string {

	file, err := os.Open(usr_config_home + "/saps/sapsrc")
	if err != nil {
		log.Fatal(err)
	}

	// Generate set
	set := generateSetMap(file)

	// Override current set with vars form local configs
	if newProject {
		set["PROJECT"] = projectName
	}

	if hasLocalConfig {
		for i := len(localConfigPath) - 1; i >= 0; i-- {

			localConfigFile, err := os.Open(localConfigPath[i] + "/sapsrc")
			if err != nil {
				log.Fatal(err)
			}
			set = generateSetMap(localConfigFile)
		}
	}
	if folder == false && hasLocalConfig == false {
		fmt.Println("No local config for this project found! You sould add one")
	}
	if hasLocalConfig == false {
		newProject = true
	}
	return set
}

func generateTemplate(set map[string]string, template string) string {
	var configError bool = false
	var strConfigError string
	re, err := regexp.Compile(`(^|[^\\])\$[A-Z]+\$`)

	if err != nil {
		log.Fatal(err)
	}

	dt := time.Now()
	set["DATE"] = dt.Format("02.01.2006")

	toReplace := re.FindAllString(template, -1)

	for _, replace := range toReplace {
		if replace[0] != '$' {
			replace = replace[1:]
		}

		replace = strings.Trim(replace, "$")

		configError = true

		for key := range set {

			if replace == key {
				configError = false
			}
			strConfigError = replace
		}

		if configError {
			fmt.Println("Variable $" + strConfigError + "$ not set in config. Add this to config or escape it with \\")
		}
	}

	for key, value := range set {
		re, err := regexp.Compile("\\$" + key + "\\$")
		if err != nil {
			log.Fatal(err)
		}

		template = re.ReplaceAllString(template, value)
	}

	return template
}

func getLocalConfig(projectName string, folder bool) (bool, []string) {
	var innerFolder bool = false
	var notHome bool = true
	var localConfigPath []string

	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	dir = dir + "/" + projectName
	for notHome {
		if dir == home {
			break
		}
		_, err := os.Stat(dir + "/.saps")

		if err == nil {
			fmt.Println("nil")
			notHome = false
			innerFolder = true
			localConfigPath = append(localConfigPath, dir+"/.saps")
		}
		//if os.IsNotExist(err) {
		split := strings.Split(dir, "/")
		dir = strings.Join(split[0:len(split)-1], "/")
		//}
	}

	if innerFolder && promptNewProject && folder {
		var promptAns string
		fmt.Print("New project? [y/N]")
		fmt.Scanf("%s", &promptAns)

		if len(promptAns) > 0 {
			if strings.ToLower(promptAns)[0] == 'y' {
				//noLocalConfig = true
				innerFolder = false
				//makeLocalConfig(projectName)
				newProject = true
			}
		}
	}

	return innerFolder, localConfigPath
}

func makeLocalConfig(path string) {
	setProjectname := "set PROJECT " + path
	set["PROJECT"] = path
	path = path + "/.saps"
	localConfigFile := path + "/sapsrc"
	os.Mkdir(path, 0777)
	err := ioutil.WriteFile(localConfigFile, []byte(setProjectname), 0644)

	if err != nil {
		fmt.Println("Error generating local config file: ", err)
	}
}

func generateSetMap(file *os.File) map[string]string {

	var words []string

	scanner := bufio.NewScanner(file)
	lineNr := 1
	for scanner.Scan() {
		line := scanner.Text()
		comments := strings.Split(line, "#")

		if len(comments) > 1 {
			words = strings.Fields(comments[0])

		} else if len(strings.Fields(line)) >= 3 {
			words = strings.Fields(line)

		} else {
			fmt.Println("Error in config line: ", lineNr)
			fmt.Println("Skipping...")
			lineNr++
			continue
		}

		switch words[0] {
		case "set":
			//set[string(words[1])] = string(words[2:len(words)])
			set[string(words[1])] = strings.Join(words[2:], " ")
		}
		lineNr++
	}
	return set
}
