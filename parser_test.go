package main

import (
	"testing"
)

func TestParseArgsNoArgsLatex(t *testing.T) {

	args := []string{"mau.tex"}
	results := parseArgs(args)
	if results.fileType != "tex" {
		t.Errorf("ParseArgsNoArgsLatex failed expexted tex got %v", results.fileType)
	} else {
		t.Logf("ParseArgsNoArgsLatex sucseeded expexted tex got %v", results.fileType)
	}

	args = []string{"mau", "tex"}

	results = parseArgs(args)

	if results.fileType != "tex" {
		t.Errorf("ParseArgsNoArgsLatex failed expected tex got %v", results.fileType)
	} else {
		t.Logf("ParseArgsNoArgsLatex sucseeded expected tex got %v", results.fileType)
	}

}

func TestParseArgsNoArgsC(t *testing.T) {

	args := []string{"mau.c"}
	results := parseArgs(args)

	if results.fileType != "c" {
		t.Errorf("ParseArgsNoArgsLatex failed expexted c got %v", results.fileType)
	} else {
		t.Logf("ParseArgsNoArgsLatex sucseeded expexted c got %v", results.fileType)
	}

	args = []string{"mau", "c"}

	results = parseArgs(args)

	if results.fileType != "c" {
		t.Errorf("ParseArgsNoArgsLatex failed expected c got %v", results.fileType)
	} else {
		t.Logf("ParseArgsNoArgsLatex sucseeded expected c got %v", results.fileType)
	}
}

func TestParseARgsNoArgsGo(t *testing.T) {

	args := []string{"mau.go"}
	results := parseArgs(args)

	if results.fileType != "go" {
		t.Errorf("ParseArgsNoArgsLatex failed expexted go got %v", results.fileType)
	} else {
		t.Logf("ParseArgsNoArgsLatex sucseeded expexted go got %v", results.fileType)
	}

	args = []string{"mau", "go"}
	results = parseArgs(args)

	if results.fileType != "go" {
		t.Errorf("ParseArgsNoArgsGo failed, expected go got %v", results.fileType)
	} else {
		t.Logf("ParseArgsNoArgsGo sucseeded, expected go got %v", results.fileType)
	}
}


func TestParseArgsGitflag(t *testing.T) {
	args := []string{"mau.c", "-g"}
	results := parseArgs(args)

	if results.gitFlag != true {
		t.Errorf("ParseArgsGitflag failed. Expected true, got %v", results.gitFlag)
	} else {
		t.Logf("ParseARgsGitflag sucseeded. Expected true got %v", results.gitFlag)
	}

	args = []string{"mau", "c", "--git", "false"}
	results = parseArgs(args)

	if results.gitFlag != false {
		t.Errorf("ParseArgsGitflag failed. Expected false, got %v", results.gitFlag)
	} else {
		t.Logf("ParseARgsGitflag sucseeded. Expected false got %v", results.gitFlag)
	}
	if results.overrideGitflag != true {
		t.Errorf("ParseArgsGitFlag failed. Expected false got %v", results.overrideGitflag)
	} else {
		t.Logf("ParseArgsGitFlag sucseeded. Expected false got %v", results.overrideGitflag)
	}

	args = []string{"mau.tex", "-g", "true"}
	results = parseArgs(args)

	if results.gitFlag != true {
		t.Errorf("ParseArgsGitflag failed. Expected false, got %v", results.gitFlag)
	} else {
		t.Logf("ParseARgsGitflag sucseeded. Expected false got %v", results.gitFlag)
	}
	if results.overrideGitflag != true {
		t.Errorf("ParseArgsGitFlag failed. Expected true got %v", results.overrideGitflag)
	} else {
		t.Logf("ParseArgsGitFlag sucseeded. Expected true got %v", results.overrideGitflag)
	}
}

func TestParseArgsMakefileFlag(t *testing.T) {

	args := []string{"mau.c", "-m"}
	results := parseArgs(args)

	if results.makeFileFlag != true {
		t.Errorf("ParseArgsMakefileFlag failed. Expected true, got %v", results.makeFileFlag)
	} else {
		t.Logf("ParseARgsMakefileFlag sucseeded. Expected true, got %v", results.makeFileFlag)
	}

	args = []string{"mau", "c", "-m", "false"}
	results = parseArgs(args)

	if results.makeFileFlag != false {
		t.Errorf("ParserArgsMakefileFlag failed. Expected false, got %v", results.makeFileFlag)
	} else {
		t.Logf("ParseArgsMakefileFlag sucseeded. Expected false, got %v", results.makeFileFlag)
	}

	if results.overrideMakefileflag != true {
		t.Errorf("ParseArgsMakefileflag failed. Expected true, got %v", results.overrideMakefileflag)
	} else {
		t.Logf("ParserArgsMakefileflag sucseeded. Expected true, got %v", results.overrideMakefileflag)
	}

	args = []string{"mau", "c", "-m", "true"}
	results = parseArgs(args)

	if results.makeFileFlag != true {
		t.Errorf("ParserArgsMakefileFlag failed. Expected true, got %v", results.makeFileFlag)
	} else {
		t.Logf("ParseArgsMakefileFlag sucseeded. Expected true, got %v", results.makeFileFlag)
	}

	if results.overrideMakefileflag != true {
		t.Errorf("ParseArgsMakefileflag failed. Expected true, got %v", results.overrideMakefileflag)
	} else {
		t.Logf("ParserArgsMakefileflag sucseeded. Expected true, got %v", results.overrideMakefileflag)
	}

	args = []string{"mau", "c", "--make", "false"}
	results = parseArgs(args)

}
