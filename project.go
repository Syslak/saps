package main

/*	Project: saps
	__
|_|(_
| |__)
Author: Håvard Syslak
Date: 31.01.2021
*/
import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func makeProject(parsedArgs *arguments) {
	var hasLocalConfig bool
	var localConfigPath []string
	var path string
	var gitPath string

	template, err := ioutil.ReadFile(templatePath + "/" + parsedArgs.fileType + "/templates/" + parsedArgs.specifiedTemplate)
	if err != nil {
		fmt.Println("Error reading data", err)
		os.Exit(1)
	}

	hasLocalConfig, localConfigPath = getLocalConfig(parsedArgs.projectName, parsedArgs.isFolder)
	set := readConfig(localConfigPath, parsedArgs.projectName, hasLocalConfig, parsedArgs.isFolder)

	if parsedArgs.isFolder {
		_, err := os.Stat(parsedArgs.projectName)
		if err == nil {
			fmt.Println("Error creating directory: \nDirectory already exists", err)
			os.Exit(1)
		}

		gitPath = parsedArgs.projectName + "/"
		os.Mkdir("./"+parsedArgs.projectName, 0777)

		if parsedArgs.srcFolder {
			path = parsedArgs.projectName + "/src/"
			os.Mkdir("./"+parsedArgs.projectName+"/src", 0777)

		} else {
			path = parsedArgs.projectName + "/"
		}

		if hasLocalConfig == false {
			makeLocalConfig(parsedArgs.projectName)
		}

		if newProject {
			parsedArgs.gitignore = gitOrMake(parsedArgs.gitFlag, parsedArgs.overrideGitflag, parsedArgs.gitDefault, parsedArgs.isFolder)
			parsedArgs.makefile = gitOrMake(parsedArgs.makeFileFlag, parsedArgs.overrideMakefileflag, parsedArgs.makeDefault, parsedArgs.isFolder)

		} else {
			parsedArgs.gitignore = gitOrMake(parsedArgs.gitFlag, parsedArgs.overrideMakefileflag, parsedArgs.gitDefault, false)
			parsedArgs.makefile = gitOrMake(parsedArgs.makeFileFlag, parsedArgs.overrideMakefileflag, parsedArgs.makeDefault, false)

		}

	} else {
		path = ""
		gitPath = ""
		parsedArgs.gitignore = gitOrMake(parsedArgs.gitFlag, parsedArgs.overrideGitflag, parsedArgs.gitDefault, parsedArgs.isFolder)
		parsedArgs.makefile = gitOrMake(parsedArgs.makeFileFlag, parsedArgs.overrideMakefileflag, parsedArgs.makeDefault, parsedArgs.isFolder)

	}

	parsedTemplate := generateTemplate(set, string(template))
	if parsedArgs.specifiedFileName != "" {
		createTemplateFile(parsedArgs.fileType, path, parsedTemplate, parsedArgs.isFolder, parsedArgs.specifiedFileName)

	} else {
		createTemplateFile(parsedArgs.fileType, path, parsedTemplate, parsedArgs.isFolder, parsedArgs.projectName)
	}

	if parsedArgs.gitignore {
		cpGitignore(gitPath, parsedArgs.fileType)
	}

	if parsedArgs.makefile && parsedArgs.fileType != "latex" { // Latex has no makefile yet
		cpMakefile(gitPath, parsedArgs.fileType)
	}
}

func cpGitignore(path string, language string) {
	var gitignore []uint8

	outfile := path + "/.gitignore"
	gitIgnorePath := templatePath + language + "/.gitignore"

	_, err := os.Stat(gitIgnorePath)
	//fmt.Println(err)
	if err != nil {
		fmt.Printf(".gitignore for %s not found. Using default .gitignore \n", language)
		gitignore, err = ioutil.ReadFile(templatePath + "/gitignore.def")
		gitIgnorePath = path + "/.gitignore"
		if err != nil {
			fmt.Println(err)
			return
		}
	} else {
		gitignore, err = ioutil.ReadFile(templatePath + language + "/.gitignore")
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	err = ioutil.WriteFile(outfile, gitignore, 0644)
	if err != nil {
		fmt.Println("Error creating", path)
		fmt.Println(err)
		return
	}
}
func createTemplateFile(extention string, path string, template string, isFolder bool, name string) {
	var fileName string

	if isFolder {
		fileName = path + name + "." + extention
	} else {
		fileName = path + name + "." + extention
	}
	if _, err := os.Stat(fileName); err == nil {
		fmt.Println(fileName, "Alerady exists.")
		os.Exit(1)
	}
	file, err := os.Create(fileName)
	if err != nil {
		fmt.Println("Error creating", path)
		fmt.Println(err)
	}
	defer file.Close()

	_, err = io.WriteString(file, template)
	if err != nil {
		fmt.Println("Error creating", path)
		fmt.Println(err)
		return
	}
}

func cpMakefile(path string, language string) {
	makefile, err := ioutil.ReadFile(templatePath + "/" + language + "/" + "Makefile")
	if err != nil {
		fmt.Println(err)
		return
	}

	makeFilePath := path + "Makefile"
	err = ioutil.WriteFile(makeFilePath, makefile, 0644)
	if err != nil {
		fmt.Println("Error creating", path)
		fmt.Println(err)
		return
	}
}

// TODO: Fix this
func gitOrMake(flag bool, override bool, def bool, folder bool) bool {
	var createFile bool
	if (override && !folder) {
		createFile = true
	} else if (override && folder && !def) {
		createFile = true
	} else if (override && folder && def) {
		createFile = false
	} else if (folder && def) {
		createFile = true
	} else if (folder && !def) {
		createFile = false
	}
	// if folder {
	// 	createFile = true
	// } else {
	// 	createFile = false
	// }
	// if (override && def) {
	// 	createFile = false
	// } else if (override && !def) {
	// 	createFile = true
	// } else {
	// 	if (!def && flag && folder) {
	// 		createFile = true
	// 	} else if (def && flag && folder) {
	// 		createFile = false
	// 	}
	// }

	return createFile
}
